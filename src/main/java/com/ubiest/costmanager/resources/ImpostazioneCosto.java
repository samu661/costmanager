package com.ubiest.costmanager.resources;

import java.util.Optional;

import org.joda.time.Interval;

import lombok.Data;

@Data
public class ImpostazioneCosto {

	private ImportoCosto importoCosto;
	private Interval fascia;

	public Optional<Interval> getFascia() {
		return Optional.ofNullable(fascia);
	}

}
