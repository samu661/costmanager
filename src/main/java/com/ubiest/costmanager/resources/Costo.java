package com.ubiest.costmanager.resources;

import lombok.Data;

@Data
public class Costo {
	private ImpostazioneCosto impostazioneCosto;
}
