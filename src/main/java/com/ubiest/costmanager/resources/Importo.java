package com.ubiest.costmanager.resources;

import java.math.BigDecimal;
import java.util.Optional;

public class Importo {

	private BigDecimal importo;

	private Importo(BigDecimal importo) {
		this.importo = importo;
	}

	public Importo(double importo) {
		this.importo = new BigDecimal(importo);
	}

	public Optional<BigDecimal> getImporto() {
		return Optional.ofNullable(importo);
	}

	public static Importo sum(Importo importo1, Importo importo2) {
		BigDecimal importoDecimal1 = importo1.getImporto().orElse(BigDecimal.ZERO);
		BigDecimal importoDecimal2 = importo2.getImporto().orElse(BigDecimal.ZERO);
		return new Importo(importoDecimal1.add(importoDecimal2));
	}

	public static Importo sum(double importo1, Importo importo2) {
		BigDecimal importoDecimal1 = new BigDecimal(importo1);
		BigDecimal importoDecimal2 = importo2.getImporto().orElse(BigDecimal.ZERO);
		return new Importo(importoDecimal1.add(importoDecimal2));
	}

	public static Importo sum(Importo importo1, double importo2) {
		BigDecimal importoDecimal1 = importo1.getImporto().orElse(BigDecimal.ZERO);
		BigDecimal importoDecimal2 = new BigDecimal(importo2);
		return new Importo(importoDecimal1.add(importoDecimal2));
	}

	public static Importo sum(double importo1, double importo2) {
		BigDecimal importoDecimal1 = new BigDecimal(importo1);
		BigDecimal importoDecimal2 = new BigDecimal(importo2);
		return new Importo(importoDecimal1.add(importoDecimal2));
	}

	public static Importo sub(Importo importo1, Importo importo2) {
		BigDecimal importoDecimal1 = importo1.getImporto().orElse(BigDecimal.ZERO);
		BigDecimal importoDecimal2 = importo2.getImporto().orElse(BigDecimal.ZERO);
		return new Importo(importoDecimal1.subtract(importoDecimal2));
	}

	public static Importo sub(double importo1, Importo importo2) {
		BigDecimal importoDecimal1 = new BigDecimal(importo1);
		BigDecimal importoDecimal2 = importo2.getImporto().orElse(BigDecimal.ZERO);
		return new Importo(importoDecimal1.subtract(importoDecimal2));
	}

	public static Importo sub(Importo importo1, double importo2) {
		BigDecimal importoDecimal1 = importo1.getImporto().orElse(BigDecimal.ZERO);
		BigDecimal importoDecimal2 = new BigDecimal(importo2);
		return new Importo(importoDecimal1.subtract(importoDecimal2));
	}

	public static Importo sub(double importo1, double importo2) {
		BigDecimal importoDecimal1 = new BigDecimal(importo1);
		BigDecimal importoDecimal2 = new BigDecimal(importo2);
		return new Importo(importoDecimal1.subtract(importoDecimal2));
	}

	public static Importo mul(Importo importo1, Importo importo2) {
		BigDecimal importoDecimal1 = importo1.getImporto().orElse(BigDecimal.ZERO);
		BigDecimal importoDecimal2 = importo2.getImporto().orElse(BigDecimal.ZERO);
		return new Importo(importoDecimal1.multiply(importoDecimal2));
	}

	public static Importo mul(double importo1, Importo importo2) {
		BigDecimal importoDecimal1 = new BigDecimal(importo1);
		BigDecimal importoDecimal2 = importo2.getImporto().orElse(BigDecimal.ZERO);
		return new Importo(importoDecimal1.multiply(importoDecimal2));
	}

	public static Importo mul(Importo importo1, double importo2) {
		BigDecimal importoDecimal1 = importo1.getImporto().orElse(BigDecimal.ZERO);
		BigDecimal importoDecimal2 = new BigDecimal(importo2);
		return new Importo(importoDecimal1.multiply(importoDecimal2));
	}

	public static Importo mul(double importo1, double importo2) {
		BigDecimal importoDecimal1 = new BigDecimal(importo1);
		BigDecimal importoDecimal2 = new BigDecimal(importo2);
		return new Importo(importoDecimal1.multiply(importoDecimal2));
	}

	public static Importo div(Importo importo1, Importo importo2) {
		BigDecimal importoDecimal1 = importo1.getImporto().orElse(BigDecimal.ZERO);
		BigDecimal importoDecimal2 = importo2.getImporto().orElse(BigDecimal.ZERO);
		return new Importo(importoDecimal1.divide(importoDecimal2));
	}

	public static Importo div(double importo1, Importo importo2) {
		BigDecimal importoDecimal1 = new BigDecimal(importo1);
		BigDecimal importoDecimal2 = importo2.getImporto().orElse(BigDecimal.ZERO);
		return new Importo(importoDecimal1.divide(importoDecimal2));
	}

	public static Importo div(Importo importo1, double importo2) {
		BigDecimal importoDecimal1 = importo1.getImporto().orElse(BigDecimal.ZERO);
		BigDecimal importoDecimal2 = new BigDecimal(importo2);
		return new Importo(importoDecimal1.divide(importoDecimal2));
	}

	public static Importo div(double importo1, double importo2) {
		BigDecimal importoDecimal1 = new BigDecimal(importo1);
		BigDecimal importoDecimal2 = new BigDecimal(importo2);
		return new Importo(importoDecimal1.divide(importoDecimal2));
	}
}
