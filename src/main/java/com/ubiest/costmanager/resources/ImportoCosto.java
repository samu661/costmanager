package com.ubiest.costmanager.resources;

import lombok.Data;

@Data
public class ImportoCosto {
	private Importo importo;
	private TipoCosto tipoCosto;
}